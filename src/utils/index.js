export const convertHtmlToText = (str) => {
	str = str?.toString();
	return str?.replace(/<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&gt;/g, '');
};
