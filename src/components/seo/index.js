/* -------------------------------------------------------------------------- */
/*                            External Dependencies                           */
/* -------------------------------------------------------------------------- */
import React from 'react';
import PropTypes from 'prop-types';
import { Helmet } from 'react-helmet-async';

/* ----------------------------- SEO  PropTypes ----------------------------- */
const propTypes = {
	title: PropTypes.string,
};

const SEO = ({ title }) => {
	return (
		<Helmet>
			<title>
				{title
					? `${title} | Taikai jobs`
					: 'Taikai jobs - Above and beyond jobs'}
			</title>
		</Helmet>
	);
};

SEO.propTypes = propTypes;
export default SEO;
