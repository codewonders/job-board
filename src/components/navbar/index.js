/* -------------------------------------------------------------------------- */
/*                             External Dependency                            */
/* -------------------------------------------------------------------------- */

import React from 'react';
import { Nav, Navbar } from 'react-bootstrap';

/* --------------------------- Image Dependencies --------------------------- */
import Logo from 'assets/icons/taikai-logo.svg';
import styled from 'styled-components';

/* -------------------------- Internal Dependencies ------------------------- */

const NavbarLayout = () => {
	return (
		<HelmStyle expand="lg">
			<div className="container-fluid">
				<Navbar.Brand href="/">
					<img src={Logo} alt="Taikai Logo" />
				</Navbar.Brand>
				<Navbar.Toggle aria-controls="basic-navbar-nav" />
				<Navbar.Collapse id="basic-navbar-nav">
					<Nav className="ml-auto">
						<Nav.Link href="/">Home</Nav.Link>
					</Nav>
				</Navbar.Collapse>
			</div>
		</HelmStyle>
	);
};

const HelmStyle = styled(Navbar)`
	background: var(--light-primary);
	@media (min-width: 990px) {
		padding: 1rem 2rem;
	}
	.navbar-toggler {
		border: none;
	}
	.navbar-brand {
		img {
			width: 100px;
		}
	}
`;
export default NavbarLayout;
