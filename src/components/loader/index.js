/* -------------------------------------------------------------------------- */
/*                            External Dependencies                           */
/* -------------------------------------------------------------------------- */
import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

/* ---------------------------- Loader propTypes ---------------------------- */
const propTypes = {
	loadingText: PropTypes.string,
};

/* --------------------------- Loader defaultProps -------------------------- */
const defaultProps = {
	loadingText: 'Loading ...',
};

const Loader = ({ loadingText, ...rest }) => {
	return (
		<Wrapper {...rest}>
			<span>
				<svg
					xmlns="http://www.w3.org/2000/svg"
					xlink="http://www.w3.org/1999/xlink"
					style={{
						margin: 'auto',
						background: 'none',
						display: 'block',
						shapeRendering: 'auto',
					}}
					width="100px"
					height="100px"
					viewBox="0 0 100 100"
					preserveAspectRatio="xMidYMid"
				>
					<circle
						cx="50"
						cy="50"
						fill="none"
						stroke="#53575c"
						strokeWidth="2"
						r="16"
						strokeDasharray="75.39822368615503 27.132741228718345"
					>
						<animateTransform
							attributeName="transform"
							type="rotate"
							repeatCount="indefinite"
							dur="0.6896551724137931s"
							values="0 50 50;360 50 50"
							keyTimes="0;1"
						></animateTransform>
					</circle>
				</svg>
				<h6>{loadingText}</h6>
			</span>
		</Wrapper>
	);
};
const Wrapper = styled.div`
	height: 100%;
	display: flex;
	padding: 3rem 0;
	align-items: center;
	justify-content: center;
	h6 {
		font-size: calc(var(--font-p) + 1px);
		text-align: center;
		font-weight: 500;
		text-transform: capitalize;
		color: #848484;
	}

	svg {
		width: 80px;
		height: 80px;
		margin-bottom: 0rem !important;
		circle {
			stroke: #848484;
			stroke-width: 3px;
		}
	}
`;

Loader.defaultProps = defaultProps;

Loader.propTypes = propTypes;

export default Loader;
