/* -------------------------------------------------------------------------- */
/*                            External Dependencies                           */
/* -------------------------------------------------------------------------- */
import React, { memo } from 'react';
import styled, { css } from 'styled-components';
import PropTypes from 'prop-types';

/* --------------------------- Image Dependencies --------------------------- */
import Logo from 'assets/icons/taikai-logo.svg';

/* ---------------------------- Footer PropTypes ---------------------------- */
const propTypes = {
	discover: PropTypes.bool,
};

const Footer = memo(() => {
	return (
		<FooterWrapper>
			<div className="container">
				<div className="row">
					<div className={`col-6 col-md-3`}>
						<img src={Logo} alt="Taikai Logo" />
						<h6>
							The frontiers of remote work have become planetary, and we can
							work from Earth to a company on Mars and from Mars to a company on
							Earth
							<br />
							<br />
							Copyright © {new Date().getFullYear()}, Taikai.
						</h6>
					</div>
					<div className="col-6 col-md"></div>

					<>
						<div className="col-6 col-md ">
							<h5>Creator</h5>
							<ul className="list-unstyled quick-links">
								<li>
									<a
										href="https://codewonders.dev"
										aria-label="Navigate To Codewonders Page"
										target="_blank"
										rel="noopener noreferrer"
									>
										@codewonders
									</a>
								</li>
								<li>
									<a
										href="https://codewonders.dev/about"
										aria-label="Navigate To Codewonders Page"
										target="_blank"
										rel="noopener noreferrer"
									>
										About Codewonders
									</a>
								</li>
								<li>
									<a
										href="https://codewonders.dev/projects"
										aria-label="Navigate To Codewonders Page"
										target="_blank"
										rel="noopener noreferrer"
									>
										Codewonders Projects
									</a>
								</li>
							</ul>
						</div>

						<div className="col-6 col-md ">
							<h5>Connect</h5>
							<ul className="list-unstyled quick-links">
								<li>
									<a
										href="https://twitter.com/taikai_hq"
										aria-label="Navigate To Codewonders Twitter"
										target="_blank"
										rel="noopener noreferrer"
									>
										Twitter
									</a>
								</li>
								<li>
									<a
										href="https://github.com/adenekan41/taikai"
										aria-label="Navigate To Github"
										target="_blank"
										rel="noopener noreferrer"
									>
										Github
									</a>
								</li>
								<li>
									<a
										href="https://instagram.com/codewonders"
										aria-label="Navigate To Instagram"
										target="_blank"
										rel="noopener noreferrer"
									>
										Instagram
									</a>
								</li>
								<li>
									<a
										href="https://linkedin.com/in/codewonders"
										aria-label="Navigate To Linkedin"
										target="_blank"
										rel="noopener noreferrer"
									>
										Linkedin
									</a>
								</li>
							</ul>
						</div>
					</>
				</div>
				<p className={` text-center`}>Created by Adenekan Wonderful</p>
			</div>
		</FooterWrapper>
	);
});
const FooterWrapper = styled.footer`
	border-top: 1px solid transparent;
	${(props) =>
		props.discover &&
		css`
			position: fixed;
			bottom: 0;
			background: #fff8f0;
			width: 100%;
			padding: 0.5rem 0 0.5rem !important;
		`};
	padding: 3rem 0 1rem;
	@media (max-width: 990px) {
		display: ${(props) => props.discover && 'none'};
	}
	img {
		height: 30px;
		width: auto;
		display: block;
		margin: 0rem auto 1rem 0;
	}
	h5 {
		font-weight: 600;
		font-size: var(--font-sm);
		line-height: 22px;
		/* identical to box height, or 183% */

		color: #787878;
	}
	p {
		font-style: italic;
		font-weight: 500;
		font-size: 13px;
		line-height: 22px;
		/* identical to box height, or 220% */

		color: #787878;
		margin-top: 2rem;
		svg {
			display: inline;
			height: 12px;
			fill: red;
			margin: 0 2px;
		}
	}
	h6 {
		font-weight: 400;
		font-size: 13px;
		line-height: 25px;
		/* identical to box height, or 220% */

		color: #787878;
	}
	ul {
		li {
			a {
				font-style: normal;
				font-weight: normal;

				/* or 183% */
				font-size: 13px;
				line-height: 31px;
				color: #787878;
			}
		}
	}
`;

Footer.propTypes = propTypes;

export default Footer;
