/* -------------------------------------------------------------------------- */
/*                             External Dependency                            */
/* -------------------------------------------------------------------------- */

import React from 'react';
import styled from 'styled-components';
import { formatDistance } from 'date-fns';
import PropTypes from 'prop-types';

/* -------------------------- Image Dependencies ------------------------- */
import { ReactComponent as Location } from 'assets/icons/icon-location.svg';
import { ReactComponent as ArrowRight } from 'assets/icons/icon-arrow-right.svg';

/* ----------------------------- Card PropTypes ----------------------------- */
const propTypes = {
	data: PropTypes.object,
};

const Card = ({ data }) => {
	return (
		<Wrapper href={data.url} target="_blank" rel="noopener noreferrer">
			<div className="card">
				<div className="card-body">
					<div className="d-flex align-items-center flex-wrap">
						<img
							src={`https://remotive.io/web/image/hr.job/${data.id}/logo?unique=${data.id}`}
							alt={data.name}
						/>
						<div className="col-md mt-3  mt-lg-0">
							<p className="mb-0 card__sub-text">
								<b>{data.company_name}</b>
							</p>
							<h4 className="mb-2">{data.title}</h4>
							<div className="d-flex">
								<p>
									<Location className="mr-1" />
									Remote ({data.candidate_required_location})
								</p>
							</div>
						</div>
						<div className="col-md-3 d-flex justify-content-end">
							<p className="text-center">
								{formatDistance(new Date(data.publication_date), new Date(), {
									addSuffix: true,
								}).replace('about', '')}
							</p>
							<ArrowRight />
						</div>
					</div>
				</div>
			</div>
		</Wrapper>
	);
};

const Wrapper = styled.a`
	text-decoration: none;
	color: inherit;
	&:hover {
		color: inherit;
		text-decoration: none;
	}
	.card {
		margin-bottom: 0;
		border-bottom: 1px solid #eaeaea !important;
		border: none;
		border-radius: 1px;
		transition: all 0.7s ease;
		cursor: pointer;

		&:hover {
			background: var(--light-primary);
		}
		.card__sub-text {
			font-size: 12px;
			text-transform: capitalize;
		}
		svg {
			width: 15px;
		}
		p {
			font-size: 14px;
			margin-bottom: 0;
			color: var(--black);
			font-weight: 400;
			b {
				font-weight: 500 !important;
				color: #909090;
			}
		}
		h4 {
			font-size: 18px;
			color: var(--black);
			margin-bottom: 2px;
			font-weight: 700;
			letter-spacing: -0.2px;
		}
		img {
			width: 50px;
			height: 50px;
			mix-blend-mode: multiply;
			object-fit: contain;
			object-position: center;
			margin-right: 1rem;
		}
	}
`;

Card.propTypes = propTypes;
export default Card;
