/* -------------------------------------------------------------------------- */
/*                             External Dependency                            */
/* -------------------------------------------------------------------------- */

import React from 'react';
import { Form } from 'react-bootstrap';
import styled from 'styled-components';

/* -------------------------- Internal Dependencies ------------------------- */
import Loader from 'components/loader';
import useCategories from 'queries/useGetCategories';
import useJobs from 'queries/useJobs';
import Card from './card';

const JobsWrapper = ({ category, setCategory, search, setSearch }) => {
	const { status, data, error } = useCategories();
	const { status: jobStatus, data: jobData } = useJobs({
		search,
		category,
	});

	if (error) return 'An error has occurred: ' + error.message;

	return (
		<SectionJobs>
			<div className="container">
				<div className="row">
					<div className="col-md-3">
						<aside>
							<h4>Categories</h4>
							<div className="filter__slider">
								{status === 'loading' ? (
									<Loader />
								) : (
									data.jobs.map((_category) => (
										<Form.Check
											type="radio"
											key={_category.id}
											name="category"
											checked={category === _category.slug}
											label={_category.name}
											value={_category.slug}
											onChange={(e) => {
												setCategory(e.target.value);
												setSearch('');
											}}
										/>
									))
								)}
							</div>
						</aside>
					</div>
					<div className="col-md-9">
						<h2>
							Latest Jobs in <span>{category.replace(/-/g, ' ')}</span> (
							{jobData?.jobs?.length || '...'})
						</h2>
						{jobStatus === 'loading' ? (
							<Loader />
						) : (
							jobData.jobs.map((job) => <Card data={job} key={job.id} />)
						)}
					</div>
				</div>
			</div>
		</SectionJobs>
	);
};

const SectionJobs = styled.section`
	margin-top: 6rem;
	margin-bottom: 2rem;
	@media (max-width: 990px) {
		margin-top: 1rem;
	}
	.filter__slider {
		@media (max-width: 990px) {
			overflow: auto hidden;
			white-space: nowrap;
			margin-bottom: 1.5rem;
			display: flex;

			-webkit-box-pack: end;
			justify-content: end;
			box-shadow: rgb(0 0 0 / 13%) 16px 0px 11px -12px;
			flex-flow: row nowrap !important;
			.form-check {
				margin-right: 1.5rem;
			}
		}
	}
	h2 {
		font-weight: 700;
		font-size: calc(var(--font-md) + 5px);
		margin-bottom: 0rem;
		border-bottom: 1px solid #eaeaea;
		padding-bottom: 1.4rem;
		color: var(--black);
		span {
			text-transform: capitalize;
		}
	}
	aside {
		border-radius: 5px;
		position: sticky;
		top: 2rem;
		margin-top: 3rem;
		h4 {
			font-size: 19px;
			font-weight: 500;
			color: var(--black);
			margin-bottom: 1rem;
		}
		label {
			color: var(--black);
			font-size: 15px;
			margin-bottom: 0.4rem;
		}
	}
`;
export default JobsWrapper;
