/* -------------------------------------------------------------------------- */
/*                             External Dependency                            */
/* -------------------------------------------------------------------------- */

import React, { useState } from 'react';

import styled from 'styled-components';

/* -------------------------- Internal Dependencies ------------------------- */

/* --------------------------- Image Dependencies --------------------------- */
import Banner from 'assets/images/header-banner.png';
import { ReactComponent as Search } from 'assets/icons/icon-search.svg';
import JobsWrapper from './components/jobs-wrapper';

const Home = () => {
	const [category, setCategory] = useState('software-dev');
	const [search, setSearch] = useState('');

	return (
		<>
			<Header>
				<div className="container">
					<div className="row align-items-center">
						<div className="col-lg">
							<h1 className="mt-0">
								Your dream job is, <br />
								No more a dream.
							</h1>
							<p>
								Find the perfect job thats suits your passion today. Get the job
								you want by researching the right employer and right categories.
							</p>
							<div className="d-block d-lg-flex input__actions">
								<div className="input-group">
									<div className="input-group-prepend">
										<span className="input-group-text">
											<Search />
										</span>
									</div>
									<input
										type="text"
										className="form-control"
										placeholder={`Job title or keyword in ${category.replace(
											/-/g,
											' '
										)}`}
										value={search}
										onChange={(e) => setSearch(e.target.value)}
									/>
								</div>
							</div>
						</div>
						<div className="col-lg-1"></div>
						<div className="col-lg">
							<img src={Banner} alt="banner" className="d-none d-lg-block" />
						</div>
					</div>
				</div>
			</Header>
			<JobsWrapper
				category={category}
				setCategory={setCategory}
				search={search}
				setSearch={setSearch}
			/>
		</>
	);
};

const Header = styled.header`
	background: var(--light-primary);
	padding: 1rem 0 3rem;
	h1 {
		font-size: calc(var(--font-x-lg) - 2px);
		font-weight: 700;
		letter-spacing: -1.1px;
		line-height: 1.12;
		margin-bottom: 0.5rem;
		color: var(--black);
	}
	img {
		width: 100%;
	}
	p {
		line-height: 1.8;
		font-size: var(--font-sm);
		margin: 1.8rem 0;
		color: #4a4a4a;
	}

	.input__actions {
		overflow: hidden;
		border-radius: 4px;
		padding: 8px 13px;
		background: #fff;
		box-shadow: 0 2px 15px #00000012;
		input {
			padding: 23px 20px;
			font-size: var(--font-sm);
			border: none;
		}

		span {
			border: none;
			background: #fff;
			padding-right: 0;
			svg {
				width: 30px;
			}
		}
	}
`;

export default Home;
