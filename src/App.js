/* -------------------------------------------------------------------------- */
/*                             External Dependency                            */
/* -------------------------------------------------------------------------- */

import React from 'react';

/* -------------------------- Internal Dependencies ------------------------- */

import SEO from 'components/seo';
import Routes from './routes';

const App = () => {
	return (
		<>
			<SEO />
			<Routes />
		</>
	);
};

export default App;
