/* -------------------------------------------------------------------------- */
/*                            External Dependencies                           */
/* -------------------------------------------------------------------------- */
import { useQuery } from 'react-query';

const getJobs = async (search, category) => {
	const data = await fetch(
		`https://remotive.io/api/remote-jobs?search=${search}&category=${category}`
	).then((res) => res.json());
	return data;
};

export default function useJobs({ search = '', category = '' }) {
	return useQuery(['jobs', search, category], () => getJobs(search, category));
}
