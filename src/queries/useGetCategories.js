/* -------------------------------------------------------------------------- */
/*                            External Dependencies                           */
/* -------------------------------------------------------------------------- */
import { useQuery } from 'react-query';

const getCategories = async () => {
	const data = await fetch(
		'https://remotive.io/api/remote-jobs/categories'
	).then((res) => res.json());
	return data;
};

export default function useCategories() {
	return useQuery('categories', getCategories);
}
