/* -------------------------------------------------------------------------- */
/*                            External Dependencies                           */
/* -------------------------------------------------------------------------- */

import React, { lazy, Suspense } from 'react';
import { Route, Switch, withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import styled from 'styled-components';

/* -------------------------- Internal Dependencies ------------------------- */
import ErrorBoundary from 'components/error-boundary';
import NavLayout from '../components/navbar';
import Footer from '../components/footer';
import LogoPrimary from 'components/logo-primary';
import SkipToMain from 'components/a11y/skip-to-main';

/* ------------------------- Component Dependencies ------------------------- */
const Home = lazy(() => import('../pages/home'));

/* ---------------------------- Routes PropTypes ---------------------------- */

const propTypes = {
	location: PropTypes.any,
};

const routes = ({ location }) => (
	<Wrapper>
		<ErrorBoundary>
			<SkipToMain content="main" />
			<NavLayout />
			<main id="main">
				<Suspense fallback={<LogoPrimary />}>
					<Switch location={location}>
						<Route exact path="/" component={Home} />
					</Switch>
				</Suspense>
				<Footer />
			</main>
		</ErrorBoundary>
	</Wrapper>
);

const Wrapper = styled.div`
	.fade-enter {
		opacity: 0.6;
	}

	.fade-enter.fade-enter-active {
		opacity: 1;
		transition: opacity 0.4s ease-in;
	}

	.fade-exit {
		opacity: 1;
	}

	.fade-exit.fade-exit-active {
		opacity: 0.6;
		transition: opacity 0.4s ease-in;
	}
`;

routes.propTypes = propTypes;

export default withRouter(routes);
